# Copyright 2018.  University of Kentucky
# This file is part of DigitalSignalProcessing.

vpath %.cxx src/
vpath %.h inc/

CXX = g++
CPPFLAGS = --std=c++11 `root-config --cflags --glibs`
INCFLAGS = -Iinc -I/usr/local/pacerepov2/gcc/7.3.0/include -I/usr/local/pacerepov2/fftw/3.3.8/gcc-7.3.0/include
LDFLAGS = -lfftw3 -L/usr/local/pacerepov2/gcc/7.3.0/lib64 -L/usr/local/pacerepov2/fftw/3.3.8/gcc-7.3.0/lib -l:libsqlite3.so.0

MAIN = analyze
SOURCES = filter.cxx
OBJECTS = $(SOURCES:.cxx=.o) $(MAIN).o 
INCLUDES = $(SOURCES:.cxx=.h)
BUILD = build/
EXECS = bin/

all: $(MAIN)

$(MAIN): $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(INCFLAGS) -o $(EXECS)$@ $(addprefix $(BUILD),$(OBJECTS)) $(LDFLAGS)

$(MAIN).o : $(MAIN).cxx $(INCLUDES)
	$(CXX) $(CPPFLAGS) $(INCFLAGS) -c $< -o $(BUILD)$@ $(LDFLAGS)

%.o : %.cxx %.h
	$(CXX) $(CPPFLAGS) $(INCFLAGS) -c $< -o $(BUILD)$@ $(LDFLAGS)

$(shell mkdir -p $(BUILD) $(EXECS))

clean :
	rm -f *.o *~ $(MAIN) build/*.o

.PHONY : all clean
