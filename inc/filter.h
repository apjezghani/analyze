#include <fftw3.h>

fftw_complex* fft_trapezoid(int length, int rise_time /*in 4ns ticks*/, int flat_top /*in 4ns ticks*/, double tau /*in 4ns ticks*/);
double* fft_convolve(int length, int filterpad, short* input, fftw_complex* filter);
