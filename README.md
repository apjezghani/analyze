Analyze
=================

This project provides uses C++ code to extract a spectrum via charge integration using a standard trapezoid filter on digitized waveform data
![](doc/trapezoid.png "Trapezoid filter output") 

The analysis is implemented using an FFT convolution for portability to other architectures such as GPU

## Getting Started

The code can be compiled and integrated into a "main" function in the executable bin/DSPMethods using the following commands:

```
$ make clean
$ make
```

*Note that the library may be properly linked as -lfftw instead of -lfftw3 as indicated in the Makefile*

### Prerequisites

This code was compiled and tested on Redhat Enterprise Linux 6.7 with GNU Make 3.82 and GCC 7.3.0 using the C++11 standard. Additionally, version 3.3.8 of the FFTW libraries ([www.fftw.org](www.fftw.org)) and version 6.18.04 of Root 
([https://root.cern.ch](https://root.cern.ch)) are required.

## Acknowledgments

This material is based upon work supported by the U.S. Department of Energy, Office of Science, Office of Nuclear Physics under Award Number DE-SC0014622.

## Contributing Authors

Source code written by Aaron Jezghani.

## Copyright and license

Copyright 2018 University of Kentucky. All rights reserved.
