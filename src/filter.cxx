#include "filter.h"
#include <math.h>

fftw_complex* fft_trapezoid(int length, int rise_time /*in 4ns ticks*/, int flat_top /*in 4ns ticks*/, double tau /*in 4ns s*/)
{
  double d;
  length = length+2*rise_time+flat_top;
  double *p = new double[length];
  fftw_complex *s = new fftw_complex[length];
  double *input = new double[length];
  p[0] = s[0][0] = s[0][1] = input[0] = 0.;
  for(int i=1; i<length; i++)
    input[i] = s[i][1] = 0.;
  input[1]=1.;
  tau = 1/(exp(1./tau)-1);
  for(int i=1; i<length; i++)
    {
      d = i>=2*rise_time+flat_top ? input[i]-input[i-rise_time]-input[i-rise_time-flat_top]+input[i-2*rise_time-flat_top] : i>=rise_time+flat_top ? input[i]-input[i-rise_time]-input[i-rise_time-flat_top] : i>=rise_time ? input[i]-input[i-rise_time] : input[i];
      p[i] = p[i-1]+d;
      s[i][0] = s[i-1][0]+p[i]+tau*d;
    }
  for(int i=0; i<length; i++)
    s[i][0] = s[i][0]/(rise_time*tau);
  fftw_plan pl = fftw_plan_dft_1d(length,s,s,FFTW_FORWARD,FFTW_ESTIMATE);
  fftw_execute(pl);
  fftw_destroy_plan(pl);
  delete[] input;
  delete[] p;
  return s;
};

double* fft_convolve(int length, int filterpad, short* input, fftw_complex* filter)
{
  double *out = new double[length];
  double *temp = new double[length+(int)fabs(filterpad)];
  fftw_complex *in = new fftw_complex[length+(int)fabs(filterpad)];
  if(filterpad>0)
    {
      for(int i=0; i<filterpad; i++)
	in[i][0] = in[i][1] = 0.;
      for(int i=0; i<length; i++)
	{
	  in[i+filterpad][0] = input[i];
	  in[i+filterpad][1] = 0.;
	}
    }
  else
    {
      for(int i=0; i<length; i++)
	{
	  in[i][0] = input[i];
	  in[i][1] = 0.;
	}
      for(int i=length; i<length+(int)fabs(filterpad); i++)
	in[i][0] = in[i][1] = 0.;
    }
  fftw_plan p;
  p = fftw_plan_dft_1d(length+(int)fabs(filterpad),in,in,FFTW_FORWARD,FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  for(int i=0; i<length+(int)fabs(filterpad); i++)
    {
      temp[i] = in[i][0]*filter[i][1]+in[i][1]*filter[i][0];
      in[i][0] = in[i][0]*filter[i][0]-in[i][1]*filter[i][1];
      in[i][1] = temp[i];
    }
  p = fftw_plan_dft_1d(length+(int)fabs(filterpad),in,in,FFTW_BACKWARD,FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  for(int i=0; i<length; i++)
    out[i] = in[i+(int)fabs(filterpad)][0]/(double)(length+fabs(filterpad));
  delete[] in;
  delete[] temp;
  return out;
}
