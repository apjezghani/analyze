#include "filter.h"

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <chrono>
#include <ctime>

#include "TH1F.h"
#include "TCanvas.h"
#include "TString.h"

void histogram(int RT, int FT, int num_files, char** filelist)
{
  std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now(); 

  TString title="Unknown Sample";
  TH1F *spect = new TH1F("Spectrum Details",title.Data(),2000,0,1000);

  short wf[3500];
  TCanvas *c1 = new TCanvas();
  unsigned int events=0;

  std::ifstream fin;

  for(int i=0; i<num_files; i++)
  {
    fin.open(filelist[i+3],std::ios::binary);
    short raw[3500];
    double *energy;
    while(fin.read((char*)raw,7000))
    {
      for(int j=0; j<3500; j++)
	    raw[j]+=114.962;
      energy = fft_convolve(3500,2*RT+FT,raw,fft_trapezoid(3500,RT,FT,1250));
      spect->Fill(17.*energy[1440]/80.);
      delete[] energy;
      ++events;
    }
    fin.close();
  }
  
  std::cout<<events<<" events detected."<<std::endl;
  spect->GetXaxis()->SetTitle("Energy [keV]");
  spect->GetYaxis()->SetTitle("Number of events");
  spect->Draw("hist");
  c1->SetLogy();
  c1->GetCanvas()->SaveAs("spectrum.jpeg");
  
  std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);
  std::cout<<"Analysis took "<<time_span.count()<<" seconds."<<std::endl;
}


# ifndef __CINT__
int main(int argc, char** argv)
{
  int RT=atoi(argv[1]), FT=atoi(argv[2]);
  histogram(RT,FT,argc-3,argv);
  return 0;
}
# endif
